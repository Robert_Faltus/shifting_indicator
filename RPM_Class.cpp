#include "RPM_Class.h"

RPM_monitor::RPM_monitor() {
	rpmStatus = RPM_UNKNOWN;
	RPM_Buffer = 0;
}

void RPM_monitor::serialReading() {
	while (Serial.available()) {
		RPM_Buffer = (uint16_t)Serial.parseInt();
	}
}
void RPM_monitor::performCalculation() {
	switch (rpmStatus) {
	case RPM_UNKNOWN:
		if (RPM_Buffer > RPM_THRESHOLD) {
			signalizationOFF();
			rpmStatus = RPM_ABOVE_THRESHOLD;
		}
		else if (RPM_Buffer > ENGINE_STOPED && RPM_Buffer <= RPM_THRESHOLD) {
			signalizationON();
			rpmStatus = RPM_BELOW_THRESHOLD;
		}
		else {
			signalizationOFF();
			rpmStatus = RPM_UNKNOWN;
		}
		break;
	case RPM_BELOW_THRESHOLD:
		if (RPM_Buffer >= RPM_TOLERANCE) {
			signalizationOFF();
			rpmStatus = RPM_ABOVE_THRESHOLD;
		}
		else if (RPM_Buffer > ENGINE_STOPED && RPM_Buffer < RPM_TOLERANCE) {
			signalizationON();
			rpmStatus = RPM_BELOW_THRESHOLD;
		}
		else {
			signalizationOFF();
			rpmStatus = RPM_UNKNOWN;
		}
		break;
	case RPM_ABOVE_THRESHOLD:
		if (RPM_Buffer > ENGINE_STOPED && RPM_Buffer <= RPM_THRESHOLD) {
			signalizationON();
			rpmStatus = RPM_BELOW_THRESHOLD;
		}
		else if (RPM_Buffer > RPM_THRESHOLD) {
			signalizationOFF();
			rpmStatus = RPM_ABOVE_THRESHOLD;
		}
		else {
			signalizationOFF();
			rpmStatus = RPM_UNKNOWN;
		}
		break;
	default:
		break;
	}
}

void RPM_monitor::signalizationON() {
	digitalWrite(SIGNAL_PIN, HIGH);
}
void RPM_monitor::signalizationOFF() {
	digitalWrite(SIGNAL_PIN, LOW);
}