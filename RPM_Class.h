#pragma once
#include <Arduino.h>
#include "ProjectDefines.h"

class RPM_monitor {
private:
	uint16_t RPM_Buffer;
	RPM_STATUS rpmStatus;

public:
	RPM_monitor();

	void serialReading();
	void performCalculation();

private:
	void signalizationON();
	void signalizationOFF();
};