/*
    Name:       shiftingIndicator.ino
    Created:	15.8.2018 13:25:37
    Author:     JSC-LENOVO-PC\JSC-LENOVO
*/
#include "ProjectDefines.h"
#include "RPM_Class.h"

RPM_monitor rpm;

void setup()
{
	Serial.begin(BAUDRATE);
	pinMode(SIGNAL_PIN, OUTPUT);
	Serial.println("Starting recieving");
}

void loop()
{	
	rpm.serialReading();
	rpm.performCalculation();
}
